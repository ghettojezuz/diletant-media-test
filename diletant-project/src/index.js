// JS - ./js/index.js

// SCSS/CSS
import './assets/scss/main.scss';
import "bootstrap-4-grid/css/grid.min.css";

import React from "react";
import {Provider} from "react-redux";
import App from "./App";
import {store} from "./App";
import ReactDOM from "react-dom";
import { BrowserRouter as Router} from 'react-router-dom';


ReactDOM.render((
    <Provider store={store}>
        <Router>
            <App/>
        </Router>
    </Provider>
), document.getElementById("root"));