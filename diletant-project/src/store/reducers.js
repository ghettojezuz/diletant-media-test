import {combineReducers} from "redux";
import {SET_SEARCH_VALUE, SET_SHOWN_CARDS, SET_SHOWN_PAGES, SET_SORTING, SET_WHAT, SET_WHEN, SET_WHERE} from "./actions"
import {cardList} from "../utils/CardsData";

const defaultStateCards = {
    cardList,
    shownPages: 1,
    shownCards: 12,
};

export const cardsReducer = (state = defaultStateCards, action) => {
    switch (action.type) {
        case SET_SHOWN_PAGES:
            return {
                ...state,
                shownPages: action.payload
            };
        case SET_SHOWN_CARDS:
            return {
                ...state,
                shownCards: action.payload
            };
    }
    return state;
};

const defaultStateSearch = {
    searchValue: "",
};

export const searchReducer = (state = defaultStateSearch, action) => {
    switch (action.type) {
        case SET_SEARCH_VALUE:
            return {
                ...state,
                searchValue: action.payload
            };
    }
    return state;
};

const defaultStateFilter = {
    sorting: "Новое",
    what: "Все",
    where: "Везде",
    when: "∞"
};

export const filterReducer = (state = defaultStateFilter, action) => {
    switch (action.type) {
        case SET_SORTING:
            return {
                ...state,
                sorting: action.payload
            };
        case SET_WHAT:
            return {
                ...state,
                what: action.payload
            };
        case SET_WHERE:
            return {
                ...state,
                where: action.payload
            };
        case SET_WHEN:
            return {
                ...state,
                when: action.payload
            };
    }
    return state;
};

export default combineReducers({
    cards: cardsReducer,
    search: searchReducer,
    filter: filterReducer,
});