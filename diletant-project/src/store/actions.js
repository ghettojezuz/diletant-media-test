export const SET_SHOWN_PAGES = "SET_SHOWN_PAGES";
export const SET_SHOWN_CARDS = "SET_SHOWN_CARDS";
export const SET_SEARCH_VALUE = "SET_SEARCH_VALUE";
export const SET_SORTING = "SET_SORTING";
export const SET_WHAT = "SET_WHAT";
export const SET_WHERE = "SET_WHERE";
export const SET_WHEN = "SET_WHEN";


// CARD ACTIONS
export const setShownPages = shownPages => ({
    type: SET_SHOWN_PAGES,
    payload: shownPages
});

export const setShownCards = shownCards => ({
    type: SET_SHOWN_CARDS,
    payload: shownCards
});

// SEARCH ACTIONS
export const setSearchValue = searchValue => ({
    type: SET_SEARCH_VALUE,
    payload: searchValue
});

// FILTER ACTIONS
export const setSorting = sorting => ({
    type: SET_SORTING,
    payload: sorting
});

export const setWhat = what => ({
    type: SET_WHAT,
    payload: what
});

export const setWhere = where => ({
    type: SET_WHERE,
    payload: where
});

export const setWhen = when => ({
    type: SET_WHEN,
    payload: when
});