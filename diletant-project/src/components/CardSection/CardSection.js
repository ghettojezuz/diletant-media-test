import React from 'react';
import CardPageContainer from "../CardPage/CardPageContainer";
import ButtonContainer from "../Button/ButtonContainer";

const CardSection = ({shownPages, shownCards, cardPages, cardsLength, handleClick}) => {

    return (
        <>
            <section className="card-section">
                    {cardPages.slice(0, shownPages).map((cardList, index) => {
                        return (
                            <div className="card-page" key={index}>
                                <CardPageContainer cardList={cardList}/>
                            </div>
                        )
                    })}

                <div className="show-more">
                    <div className="show-more__count">Показано {shownCards} из {cardsLength} карточек</div>
                    {shownCards < cardsLength && (
                        <ButtonContainer text="Больше материалов"
                                         onClick={handleClick}/>
                    )}

                </div>
            </section>
        </>
    )
};

export default CardSection;