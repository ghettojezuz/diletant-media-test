import React, {useEffect, useState} from 'react';
import CardSection from "./CardSection";
import {connect} from "react-redux";
import {chunkArray} from "../../utils/chunkArray";
import useWindowSize from "../../utils/useWindowWidth";
import {getIndexToSplitIntoPages} from "../../utils/Split";
import {setSearchValue} from "../../store/actions";

const CardSectionContainer = ({cardList, searchValue, sorting, what, where, when}) => {

    const windowWidth = useWindowSize();
    const [indexToSplit, setIndexToSplit] = useState(getIndexToSplitIntoPages(windowWidth));
    const [currentDisplayed, setCurrentDisplayed] = useState(cardList);
    const [cardsLength, setCardsLength] = useState(cardList.length);
    const [shownPages, setShownPages] = useState(1);
    const [shownCards, setShownCards] = useState(indexToSplit);
    const [cardPages, setCardPages] = useState(chunkArray(currentDisplayed, indexToSplit));

    useEffect(() => {
        setIndexToSplit(getIndexToSplitIntoPages(windowWidth));
    }, [windowWidth]);

    useEffect(() => {
        setShownPages(1);
        setShownCards(indexToSplit);

    }, [indexToSplit]);

    useEffect(() => {
        setCardPages(chunkArray(currentDisplayed, indexToSplit));
    }, [indexToSplit, currentDisplayed]);

    // useEffect for searching, sorting and filter
    useEffect(() => {
        function sortByDate(o1, o2){
            let date1 = new Date(o1.date).getTime();
            let date2 = new Date(o2.date).getTime();
            return date1 < date2 ? 1 : -1;
        }

        function sortByViews(o1, o2) {
            let view1 = o1.tags.watches;
            let view2 = o2.tags.watches;
            return view1 < view2 ? 1 : -1;
        }

        // SORTING
        var sortedCardList;
        if (sorting === "Новое") {
            sortedCardList = cardList.sort(sortByDate);
        } else if (sorting === "Лучшее") {
            sortedCardList = cardList.sort(sortByViews);
        }

        // SEARCHING
        const searchedCardList = sortedCardList.filter((item) => item.title.toLowerCase().includes(searchValue.toLowerCase()));

        // FILTER
        var whatValue = what, whereValue = where, whenValue = when;
        if (what === "Все" || what === "Что") {
            whatValue = "";
        }
        if (where === "Везде" || where === "Где") {
            whereValue = "";
        }
        if (when === "∞" || when === "Когда") {
            whenValue = "";
        }
        const filteredCardList = searchedCardList.filter((item) => item.tags.what.includes(whatValue)  &&
                                                                 item.tags.where.includes(whereValue) &&
                                                                 item.tags.when.includes(whenValue));


        setCurrentDisplayed(filteredCardList);
        setCardsLength(filteredCardList.length);
        setCardPages(chunkArray(currentDisplayed, indexToSplit));
        setShownPages(1);
    }, [searchValue, sorting, what, where, when]);


    useEffect(() => {
        setShownCards(cardPages[0] === undefined ? 0 : cardPages[0].length);
    }, [cardPages]);


    const handleClick = () => {
        setShownPages(shownPages + 1);
        setShownCards(shownCards + cardPages[shownPages].length);
    };

    return <CardSection shownPages={shownPages}
                        shownCards={shownCards}
                        cardPages={cardPages}
                        cardsLength={cardsLength}
                        handleClick={handleClick}/>
};

const mapStateToProps = state => {
    return {
        cardList: state.cards.cardList,
        searchValue: state.search.searchValue,
        sorting: state.filter.sorting,
        what: state.filter.what,
        where: state.filter.where,
        when: state.filter.when,
    };
};

const mapDispatchToProps = {
    setSearchValue,
};

export default connect(mapStateToProps, mapDispatchToProps)(CardSectionContainer);