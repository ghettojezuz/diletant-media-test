import React from 'react';
import CardPage from "./CardPage";
import useWindowSize from "../../utils/useWindowWidth";
import {getIndexToSplitIntoParts} from "../../utils/Split"


const CardPageContainer = ({cardList}) => {

    const windowWidth = useWindowSize();
    const indexToSplit = getIndexToSplitIntoParts(windowWidth)[0];
    const indexOfLargeCard = getIndexToSplitIntoParts(windowWidth)[1];
    const firstCardPagePart = cardList.slice(0, indexToSplit);
    const secondCardPagePart = cardList.slice(indexToSplit, cardList.length);

    return <CardPage firstCardPagePart={firstCardPagePart}
                     secondCardPagePart={secondCardPagePart}
                     indexOfLargeCard={indexOfLargeCard}/>
};


export default CardPageContainer;