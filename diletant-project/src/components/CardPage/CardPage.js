import React from 'react';
import CardContainer from "../Card/CardContainer";
const classNames = require("classnames");

const CardPage = ({firstCardPagePart, secondCardPagePart, indexOfLargeCard}) => {

    return (
        <>
            <div className="cards-container">
                <div className="cards-row">
                    {firstCardPagePart.map((card, index) => {
                        return (
                            <div className={classNames(index === indexOfLargeCard ? "card-column--lg" : "card-column")}
                                 key={card.id}>
                                <CardContainer title={card.title}
                                               text={card.text}
                                               img={card.img}
                                               tags={card.tags}/>
                            </div>
                        )
                    })}
                </div>
            </div>
            <section className="a-section">
                <div className="container">
                    <p className="a-section__text">реклама на diletant.media</p>
                    <a className="a-section__link" href="#">
                        <img src="../../assets/img/banner.jpg" alt=""/>
                    </a>
                </div>
            </section>
            <div className="cards-container">
                <div className="cards-row">
                    {secondCardPagePart.map((card) => {
                        return (
                            <div className="card-column" key={card.id}>
                                <CardContainer title={card.title}
                                               text={card.text}
                                               img={card.img}
                                               tags={card.tags}/>
                            </div>
                        )
                    })}
                </div>
            </div>
        </>
    )
};

export default CardPage;