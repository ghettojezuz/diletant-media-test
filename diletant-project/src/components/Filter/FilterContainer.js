import React, {useState} from 'react';
import Filter from "./Filter";
import useWindowWidth from "../../utils/useWindowWidth";
import {sortItemList, whatItemList, whenItemList, whereItemList} from "../../utils/SelectData";
import {setSorting, setWhat, setWhen, setWhere} from "../../store/actions";
import {connect} from "react-redux";

const FilterContainer = ({setSorting, setWhat, setWhere, setWhen}) => {

    const [isOpen, setIsOpen] = useState(false);
    const windowWidth = useWindowWidth();

    const handleToggle = () => {
        setIsOpen(!isOpen);
    };

    return <Filter sortItemList={sortItemList}
                   whatItemList={whatItemList}
                   whenItemList={whenItemList}
                   whereItemList={whereItemList}
                   isMobile={windowWidth <= 768}
                   isOpen={isOpen}
                   handleToggle={handleToggle}
                   setSorting={setSorting}
                   setWhat={setWhat}
                   setWhere={setWhere}
                   setWhen={setWhen}/>
};

const mapStateToProps = state => {
    return {

    };
};

const mapDispatchToProps = {
    setSorting,
    setWhat,
    setWhere,
    setWhen
};

export default connect(mapStateToProps, mapDispatchToProps)(FilterContainer);