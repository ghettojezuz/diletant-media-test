import React from 'react';
import SelectContainer from "../Select/SelectContainer";

const classNames = require("classnames");

const Filter = ({sortItemList, whatItemList, whereItemList, whenItemList, isMobile, isOpen, handleToggle, setSorting, setWhat, setWhere, setWhen}) => {

    return (
        <>
            <section className="filter-section">
                <div className={classNames(isMobile ? "container-fluid" : "container")}>
                    <div className="filter-widget">
                        <div className="sorting-wrapper">
                            <span>Сортировка: </span>
                            <SelectContainer onChange={value => setSorting(value)}
                                             itemList={sortItemList}
                                             firstActive={true}/>
                        </div>
                        <div className="filter-wrapper">
                            <SelectContainer onChange={value => setWhat(value)}
                                             itemList={whatItemList}
                                             initialValue="Что"/>
                            <SelectContainer onChange={value => setWhere(value)}
                                             itemList={whereItemList}
                                             initialValue="Где"/>
                            <SelectContainer onChange={value => setWhen(value)}
                                             itemList={whenItemList}
                                             initialValue="Когда"
                                             minWidth="120px"/>
                        </div>
                        <div className="filter-mobile-toggler" onClick={handleToggle}>
                            <svg width="25" height="20" viewBox="0 0 25 20" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <line x1="1" y1="3" x2="24" y2="3" stroke="#556274" strokeWidth="2"
                                      strokeLinecap="round" strokeLinejoin="round"/>
                                <line x1="1" y1="10" x2="24" y2="10" stroke="#556274" strokeWidth="2"
                                      strokeLinecap="round" strokeLinejoin="round"/>
                                <line x1="1" y1="17" x2="24" y2="17" stroke="#556274" strokeWidth="2"
                                      strokeLinecap="round" strokeLinejoin="round"/>
                                <circle cx="17" cy="17" r="2" fill="white" stroke="#556274" strokeWidth="2"/>
                                <circle cx="7" cy="10" r="2" fill="white" stroke="#556274" strokeWidth="2"/>
                                <circle cx="17" cy="3" r="2" fill="white" stroke="#556274" strokeWidth="2"/>
                            </svg>
                        </div>
                        <div className={classNames("filer-mobile-wrapper", isOpen ? "active" : "")}>
                            <SelectContainer onChange={value => setWhat(value)}
                                             itemList={whatItemList}
                                             initialValue="Что"/>
                            <SelectContainer onChange={value => setWhere(value)}
                                             itemList={whereItemList}
                                             initialValue="Где"/>
                            <SelectContainer onChange={value => setWhen(value)}
                                             itemList={whenItemList}
                                             initialValue="Когда"
                                             minWidth="120px"/>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
};

export default Filter;