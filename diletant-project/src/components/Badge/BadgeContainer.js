import React from 'react';
import Badge from "./Badge";

const BadgeContainer = ({icon, text}) => {

    return <Badge icon={icon ? icon : false}
                  text={text}/>
};

export default BadgeContainer;