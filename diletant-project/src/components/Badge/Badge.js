import React from 'react';

const Badge = ({icon, text}) => {

    return (
        <>
            <div className="badge">
                { icon && (<div className="badge__icon"><img src={icon} alt=""/></div>)}
                <div className="badge__text">{text}</div>
            </div>
        </>
    )
};

export default Badge;