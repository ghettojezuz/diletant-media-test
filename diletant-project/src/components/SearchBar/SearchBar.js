import React from 'react';
import {Link} from "react-router-dom";

const SearchBar = ({value, onChange}) => {

    return (
        <>
            <div className="search-bar">
                <input className="search-bar__input"
                       type="text"
                       value={value}
                       onChange={event => onChange(event)}
                       placeholder="Что ищем?"/>
                <Link to="/animation" className="search-bar__btn">
                    <svg width="42" height="43" viewBox="0 0 42 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="29.7021" y="10.1875" width="3" height="29" rx="1.5" transform="rotate(45 29.7021 10.1875)" fill="#556274"/>
                        <rect x="9.19629" y="12.3086" width="3" height="29" rx="1.5" transform="rotate(-45 9.19629 12.3086)" fill="#556274"/>
                    </svg>
                </Link>
            </div>
        </>
    )
};

export default SearchBar;