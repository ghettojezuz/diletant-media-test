import React from 'react';
import SearchBar from "./SearchBar";
import {setSearchValue} from "../../store/actions";
import {connect} from "react-redux";

const SearchBarContainer = ({searchValue, setSearchValue}) => {

    const onChange = (event) => {
        let value = event.target.value;
        setSearchValue(value);
    };

    return <SearchBar value={searchValue}
                      onChange={onChange}/>
};

const mapStateToProps = state => {
    return {
        searchValue: state.cards.searchValue,
    };
};

const mapDispatchToProps = {
    setSearchValue,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBarContainer);