import React from 'react';
const classNames = require("classnames");

const Select = React.forwardRef((props, ref) => {
    const {value, isOpen, getItemList, handleClick, minWidth, firstActive, haveChosen} = props;
    return (
        <>
            <ul className={classNames(isOpen ? "select active" : "select", firstActive || haveChosen ? "chosen" : "")}
                onClick={handleClick}
                style={!minWidth ? {minWidth: "100px"} : {minWidth: minWidth}}
                ref={ref}>
                <div className="select__text">
                    {value}
                </div>
                {getItemList}
            </ul>
        </>
    )
});

export default Select;