import React, {useState, useEffect, useRef} from 'react';
import Select from "./Select";
const classNames = require("classnames");

const SelectContainer = ({initialValue, onChange, itemList, minWidth, firstActive}) => {
    const [value, setValue] = useState(!initialValue ? itemList[0] : initialValue);
    const [isOpen, setIsOpen] = useState(false);
    const [haveChosen, setHaveChosen] = useState(firstActive);
    const selectRef = useRef(null);

    const handleClickOutside = e => {
        if (!selectRef.current.contains(e.target)) {
            setIsOpen(false);
        }
    };

    useEffect(() => {
        document.addEventListener('mousedown', handleClickOutside);
        return () => document.removeEventListener('mousedown', handleClickOutside);
    }, []);

    const handleClick = () => {
        setIsOpen(!isOpen);
    };

    const handleClickOnOption = event => {
      var currentValue = event.currentTarget.textContent;
      if (event.target.dataset.index === "1") {
          if (initialValue) currentValue = initialValue;
          setHaveChosen(false);
      } else {
          setHaveChosen(true);
      }
      setValue(currentValue);
      onChange(currentValue);
    };

    const getItemList = props => {
        const list = props.map((item, index) => (
            <div onClick={event => handleClickOnOption(event)}
                 className={classNames("select__item")}
                 key={index}
                 data-index={index+1}>
                {item}
            </div>
        ));
        return ( <li className="select__items"> { list } </li> )
    };

    return <Select value={value}
                   isOpen={isOpen}
                   getItemList={getItemList(itemList)}
                   handleClick={handleClick}
                   minWidth={minWidth}
                   ref={selectRef}
                   firstActive={!firstActive ? false : firstActive}
                   haveChosen={haveChosen}/>
};

export default SelectContainer;