import React from 'react';
import Button from "./Button";

const ButtonContainer = ({text, onClick}) => {

    return <Button text={text}
                   onClick={onClick}/>
};

export default ButtonContainer;