import React from 'react';
import Card from "./Card";

const CardContainer = ({title, text, img, tags}) => {

    return <Card title={title}
                 text={text}
                 img={img}
                 tags={tags}/>
};

export default CardContainer;