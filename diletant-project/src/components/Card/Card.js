import React from 'react';
import BadgeContainer from "../Badge/BadgeContainer";

const Card = ({title, text, img, tags}) => {

    return (
        <>
            <div className="card">
                <div className="card-main" style={{backgroundImage: `url(${img})`}}>
                    <div className="card-main-content">
                        <div className="card-main-content__separator">
                            <span className="separator-logo">
                                <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M7.49324 1.09091H3.25969C3.17854 1.09091 3.15149 1.06364 3.15149 0.981818V0H0V4.09091C0 4.21364 0.0676285 4.26818 0.202885 4.26818H1.67719L5.50496 15H7.49324H9.49504L13.3093 4.26818H14.7971C14.9324 4.26818 15 4.21364 15 4.09091V0H11.8485V0.981818C11.8485 1.06364 11.8079 1.09091 11.7268 1.09091H7.49324ZM7.49324 10.8545C7.41208 10.8545 7.34445 10.8 7.3174 10.7045L5.12624 4.52727C5.09919 4.45909 5.09919 4.40455 5.13977 4.35C5.18034 4.29545 5.23445 4.26818 5.30207 4.26818H7.49324H9.69793C9.76555 4.26818 9.80613 4.29545 9.84671 4.35C9.88729 4.40455 9.90081 4.45909 9.87376 4.52727L7.6826 10.7045C7.65555 10.8 7.58792 10.8545 7.49324 10.8545Z"
                                        fill="white"/>
                                </svg>
                            </span>
                        </div>
                        <h3 className="card-main-content__title">{title}</h3>
                    </div>
                </div>
                <div className="card-description">
                    <div className="card-description-content">
                        <h3 className="card-description-content__title"> {title}</h3>
                        <p className="card-description-content__text">{text}</p>
                    </div>
                </div>
                <div className="card-badges">
                    <BadgeContainer text={tags.what}/>
                    <BadgeContainer text={tags.when}/>
                    <BadgeContainer text={tags.where}/>
                </div>
                <div className="card-badges--with-icon">
                    <BadgeContainer text={tags.what} icon="../../assets/img/book_icon.png"/>
                    <BadgeContainer text={tags.watches} icon="../../assets/img/eyes_icon.png"/>
                    <BadgeContainer text={tags.time} icon="../../assets/img/time_icon.png"/>
                </div>
            </div>
        </>
    )
};

export default Card;