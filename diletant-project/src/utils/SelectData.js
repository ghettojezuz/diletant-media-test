export const sortItemList = ["Новое", "Лучшее"];
export const whatItemList = ["Все", "Статьи", "Тесты", "Сборники"];
export const whereItemList = ["Везде", "Европа", "Азия", "Америка", "Африка", "Австралия", "Другое"];
export const whenItemList = ["∞", "До VIII до н.э.", "VII до н.э. - V н.э.", "V - XIV в.", "XV - XVII", "XVII - XX", "XX - XXI"];
