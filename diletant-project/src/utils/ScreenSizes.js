export const phoneWidth = 375;
export const tabletWidth = 768;
export const smDesktopWidth = 1024;
export const desktopWidth = 1440;