import {whatItemList, whenItemList, whereItemList} from "./SelectData";

export const cardList = [
    {
        id: 0,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_1.jpg",
        date: new Date("2020-05-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Америка",
            when: whenItemList[1],
            watches: 78,
            time: "15 минут",
        }
    },
    {
        id: 1,
        title: "Кристина Шведская: королева, промотавшая королевство",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое",
        img: "../../assets/img/bg_2.jpg",
        date: new Date("2020-07-05T17:25:45Z"),
        tags: {
            what: "Тесты",
            where: "Европа",
            when: whenItemList[5],
            watches: 12010,
            time: "15 минут",
        }
    },
    {
        id: 2,
        title: "Как Ельцин хотел забрать Крым",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_6.jpg",
        date: new Date("2020-03-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Африка",
            when: whenItemList[4],
            watches: 3466,
            time: "15 минут",
        }
    },
    {
        id: 3,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_2.jpg",
        date: new Date("2020-07-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Азия",
            when: whenItemList[4],
            watches: 75644,
            time: "15 минут",
        }
    },{
        id: 4,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_1.jpg",
        date: new Date("2020-05-03T12:25:45Z"),
        tags: {
            what: "Сборники",
            where: "Азия",
            when: whenItemList[3],
            watches: 12345,
            time: "15 минут",
        }
    },
    {
        id: 5,
        title: "Кристина Шведская: королева, промотавшая королевство",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое",
        img: "../../assets/img/bg_2.jpg",
        date: new Date("2021-05-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Европа",
            when: whenItemList[5],
            watches: 3345,
            time: "15 минут",
        }
    },
    {
        id: 6,
        title: "Инки, майя или ацтеки?",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_3.jpg",
        date: new Date("2020-05-11T17:25:45Z"),
        tags: {
            what: "Сборники",
            where: "Азия",
            when: whenItemList[6],
            watches: 7888,
            time: "15 минут",
        }
    },
    {
        id: 7,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_4.jpg",
        date: new Date("2020-01-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Америка",
            when: whenItemList[1],
            watches: 6546,
            time: "15 минут",
        }
    },{
        id: 8,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_1.jpg",
        date: new Date("2020-05-09T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Европа",
            when: whenItemList[6],
            watches: 999,
            time: "15 минут",
        }
    },
    {
        id: 9,
        title: "Кристина Шведская: королева, промотавшая королевство",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое",
        img: "../../assets/img/bg_2.jpg",
        date: new Date("2011-05-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Азия",
            when: whenItemList[3],
            watches: 888,
            time: "15 минут",
        }
    },
    {
        id: 10,
        title: "Инки, майя или ацтеки?",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_3.jpg",
        date: new Date("2020-05-03T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Европа",
            when: whenItemList[4],
            watches: 111,
            time: "15 минут",
        }
    },
    {
        id: 11,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_4.jpg",
        date: new Date("2020-05-02T17:25:45Z"),
        tags: {
            what: "Сборники",
            where: "Европа",
            when: whenItemList[4],
            watches: 222,
            time: "15 минут",
        }
    },{
        id: 12,
        title: "Кристина Шведская: королева, промотавшая королевство",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое",
        img: "../../assets/img/bg_2.jpg",
        date: new Date("2020-05-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Европа",
            when: whenItemList[4],
            watches: 333,
            time: "15 минут",
        }
    },
    {
        id: 13,
        title: "Инки, майя или ацтеки?",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_3.jpg",
        date: new Date("2020-05-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Азия",
            when: whenItemList[4],
            watches: 666,
            time: "15 минут",
        }
    },
    {
        id: 14,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_4.jpg",
        date: new Date("2020-05-07T17:25:45Z"),
        tags: {
            what: "Тесты",
            where: "Европа",
            when: whenItemList[3],
            watches: 555,
            time: "15 минут",
        }
    },{
        id: 15,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_6.jpg",
        date: new Date("2021-01-02T17:25:45Z"),
        tags: {
            what: "Сборники",
            where: "Азия",
            when: whenItemList[3],
            watches: 2113,
            time: "15 минут",
        }
    },
    {
        id: 16,
        title: "Кристина Шведская: королева, промотавшая королевство",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое",
        img: "../../assets/img/bg_2.jpg",
        date: new Date("2020-05-02T13:23:45Z"),
        tags: {
            what: "Статьи",
            where: "Африка",
            when: whenItemList[5],
            watches: 76556,
            time: "15 минут",
        }
    },
    {
        id: 17,
        title: "Как Ельцин хотел забрать Крым",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_6.jpg",
        date: new Date("2013-05-02T17:25:45Z"),
        tags: {
            what: "Сборники",
            where: "Европа",
            when: whenItemList[3],
            watches: 1244,
            time: "15 минут",
        }
    },
    {
        id: 18,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_4.jpg",
        date: new Date("2020-05-01T17:25:45Z"),
        tags: {
            what: "Тесты",
            where: "Африка",
            when: whenItemList[2],
            watches: 234455,
            time: "15 минут",
        }
    },{
        id: 19,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_1.jpg",
        date: new Date("2020-05-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Америка",
            when: whenItemList[4],
            watches: 567,
            time: "15 минут",
        }
    },
    {
        id: 20,
        title: "Кристина Шведская: королева, промотавшая королевство",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое",
        img: "../../assets/img/bg_2.jpg",
        date: new Date("2020-06-07T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Австралия",
            when: whenItemList[1],
            watches: 345,
            time: "15 минут",
        }
    },
    {
        id: 21,
        title: "Инки, майя или ацтеки?",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_3.jpg",
        date: new Date("2010-05-01T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Европа",
            when: whenItemList[4],
            watches: 675,
            time: "15 минут",
        }
    },
    {
        id: 22,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_6.jpg",
        date: new Date("2020-12-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Австралия",
            when: whenItemList[6],
            watches: 3212,
            time: "15 минут",
        }
    },{
        id: 23,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_6.jpg",
        date: new Date("2000-05-02T17:25:45Z"),
        tags: {
            what: "Тесты",
            where: "Америка",
            when: whenItemList[2],
            watches: 1,
            time: "15 минут",
        }
    },
    {
        id: 24,
        title: "Кристина Шведская: королева, промотавшая королевство",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое",
        img: "../../assets/img/bg_2.jpg",
        date: new Date("2020-07-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Европа",
            when: whenItemList[4],
            watches: 11,
            time: "15 минут",
        }
    },
    {
        id: 25,
        title: "Инки, майя или ацтеки?",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_3.jpg",
        date: new Date("2020-05-02T17:11:45Z"),
        tags: {
            what: "Статьи",
            where: "Другое",
            when: whenItemList[5],
            watches: 3235,
            time: "15 минут",
        }
    },
    {
        id: 26,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_4.jpg",
        date: new Date("1999-05-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Европа",
            when: whenItemList[1],
            watches: 1312,
            time: "15 минут",
        }
    },{
        id: 27,
        title: "Кристина Шведская: королева, промотавшая королевство",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое",
        img: "../../assets/img/bg_2.jpg",
        date: new Date("2020-05-02T17:25:45Z"),
        tags: {
            what: "Статьи",
            where: "Европа",
            when: whenItemList[2],
            watches: 553,
            time: "15 минут",
        }
    },
    {
        id: 28,
        title: "Инки, майя или ацтеки?",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_3.jpg",
        date: new Date("2020-03-02T17:25:45Z"),
        tags: {
            what: "Сборники",
            where: "Австралия",
            when: whenItemList[2],
            watches: 111,
            time: "15 минут",
        }
    },
    {
        id: 29,
        title: "Вытрезвитель - кошмар пролетария",
        text: "В истории немало знаковых, переломных и важных годов. И немало людей, которые с удовольствием отправились бы в прошлое, если бы могли.",
        img: "../../assets/img/bg_6.jpg",
        date: new Date("1999-10-29T14:25:45Z"),
        tags: {
            what: "Тесты",
            where: "Другое",
            when: whenItemList[4],
            watches: 123,
            time: "15 минут",
        }
    },
];