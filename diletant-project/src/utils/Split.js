import {desktopWidth, smDesktopWidth, tabletWidth} from "./ScreenSizes";

// Возвращает кол-во карточек в CardPage в зависимости от ширины экрана
export const getIndexToSplitIntoPages = (windowWidth) => {
    let indexToSplit = -1;
    if (windowWidth < tabletWidth) {
        indexToSplit = 6;
    } else if (windowWidth >= tabletWidth && windowWidth < smDesktopWidth) {
        indexToSplit = 7;
    } else if (windowWidth >= smDesktopWidth && windowWidth < desktopWidth) {
        indexToSplit = 8;
    } else if (windowWidth >= desktopWidth) {
        indexToSplit = 11;
    }
    return indexToSplit;
};

//Возвращает кол-во карточек в первой части страницы и второй, возвращает индекс большой карточки
export const getIndexToSplitIntoParts = (windowWidth) => {
    let indexToSplit = -1;
    let indexOfLargeCard = 2;
    if (windowWidth < smDesktopWidth) {
        indexToSplit = 3;
    } else if (windowWidth >= smDesktopWidth && windowWidth < desktopWidth) {
        indexToSplit = 5;
        indexOfLargeCard = 1;
    } else if (windowWidth >= desktopWidth) {
        indexToSplit = 7;
    }
    return [indexToSplit, indexOfLargeCard];
};