import React from 'react';
import {Switch, Route} from 'react-router-dom';
import {createStore} from "redux";
import rootReducer from "./store/reducers";
import SearchPage from "./pages/SearchPage";
import Animation from "./components/Animation/Animation";

export const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const App = () => {

    return (
        <>
            <Switch>
                <Route exact path='/' render={() => <SearchPage/>}/>
                <Route exact path='/animation' render={() => <Animation/>}/>
            </Switch>
        </>
    )
};

export default App;