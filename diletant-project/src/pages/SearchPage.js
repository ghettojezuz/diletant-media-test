import React from 'react';
import SearchBarContainer from "../components/SearchBar/SearchBarContainer";
import FilterContainer from "../components/Filter/FilterContainer";
import CardPageContainer from "../components/CardPage/CardPageContainer";
import CardSectionContainer from "../components/CardSection/CardSectionContainer";
import Footer from "../components/Footer/Footer";

const SearchPage = () => {

    return (
        <>
            <div className="container">
                <section className="search-section">
                    <SearchBarContainer/>
                </section>
            </div>
            <hr className="hr"/>
            <main className="main">
                <FilterContainer/>
                <CardSectionContainer/>
            </main>
            <Footer/>
        </>
    )
};

export default SearchPage;