![Diletant-image](diletant-project/src/assets/img/diletant_logo_175_37.png)
# Тестовое задание для diletant.media

Тестовое задание для diletant.media на основе React-Redux (поиск, сортировка и анимация)

## Использование

Для запуска проекта на dev-сервере необходимо: 
1.  Клонировать репозиторий (```git clone git@gitlab.com:ghettojezuz/diletant-media-test.git```)
2.  Установить ```dependencies``` и ```devDependencies``` из ```package.json```
3.  Ввести команду ```npm run dev``` в консоли

Для просмотра build-версии на хостинге firebase:
1. Перейти на https://diletant-media-test.web.app/
2. PROFIT

## Структура проекта
*  ```/diletant-project```
    *  ```/build``` (конфиги webpack)
    *  ```/dist``` (build версия проекта)
    *  ```/node-modules``` (npm пакеты)
    *  ```/src``` 
        *  ```/assets``` (различные ресурсы)
            *  ```/fonts``` (шрифты)
            *  ```/img``` (изображения)
            *  ```/scss``` (scss код)
                *  ```/components``` (scss для компонентов)
                *  ```/pages``` (scss для страниц)
                *  ```/utils``` (переменные, миксины и т.д.)
                *  ```/main.scss``` (основной файл стилей)
        * ```/components``` (react-компоненты)
            * ```/component``` 
                * ```/component.js``` (dumb компонент)
                * ```/componentContainer.js``` (smart компонент)
        * ```/pages``` (компоненты для страниц)
        * ```/store``` (redux actions and reducers)
        * ```/utils``` (сторонний код)
        * ```/App.js``` (компонент приложения)
        * ```/index.html``` (основной html)
        * ```/index.js``` (входная точка приложения)
        
## Описание "рутинного" задания

* Структура данных для карточки (файл ```/utils/CardsData.js```):
``` 
{
    id: id,
    title: title,
    text:  text,
    img: img_url,
    date: new Date("date"),
    tags: {
        what: what_filter,
        where: where_filter,
        when: when_filter,
        watches: views_count,
        time: how_much_to_read,
    }
}
```
* Поиск осуществляется по полю ```title```
* Сортировка осуществляется либо по полю ```date```, либо по полю ```tags.watches```
* Фильтрация осуществляется по полям ```what```,```where```,```when```
* Основные break-поинты ширины экрана: ```375px```, ```768px```, ```1024px```, ```1440px```

## Описание "творческого задания"
* Чтобы увидеть анимацию из творческого задания, нужно нажать на кнопку рядом с инпутом для поиска:
![Animation-access](diletant-project/src/assets/img/gitlab1.jpg)
* Реализация анимации находится в файлах ```/diletant-project/src/components/Animation/Animations.js``` и ```/diletant-project/src/assets/scss/components/animation.scss```
* P.S. Я понимаю, что можно было сделать намного интереснее и качественнее эту анимацию через canvas, но так как я с ним особо не сталкивался, то сделал как смог на данный момент

## Контакты
* 📧 Почта: [rebim@yandex.ru](mailto:rebim@yandex.ru)
* 💬 Telegram: [@ghettojezuz](t.me/ghettojezuz)